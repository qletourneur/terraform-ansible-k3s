# K3s cluster automated with Terraform and Ansible

## Prerequisites

You need to install Ansible and Terraform, they will be used to create the K3S Cluster on your Proxmox node

Tested on versions :
* Ansible core 2.14.3 and Python 3.11.2
* Terraform 1.7.5
* Debian 12 (bookworm)

This project is using the Ansible k3s project from Techno-tim :
[techno-tim/k3s-ansible](https://github.com/techno-tim/k3s-ansible)

### Variables

You will need to edit the file terraform/variables.tfvars.example :

* **Proxmox variables**

| Variable name | Description | Default value |
| --- | --- | --- |
| proxmox_endpoint | https + IP address or hostname of the Proxmox server + Port | https://xxx.xxx.xxx.xxx:8006 |
| proxmox_user | User to connect on Proxmox server | root |
| proxmox_user_password | User password to connect on Proxmox server | myverysecuredpassword |
| proxmox_node | Name of node where you want things to be created | pve |
| proxmox_datastore_images | Datastore to use on Proxmox for the images | local |
| proxmox_datastore_disks | Datastore to use on Proxmox for the VM disks | local-lvm |
| proxmox_datastore_files | Datastore to use on Proxmox for the files | local |

* **Virtual machines variables**

| Variable name | Description | Default value |
| --- | --- | --- |
| vm_ssh_public_key | SSH Public key to use in VM managed by Terraform | ssh-ed25519 xxxxxxxxxxxxx debian@debian |
| vm_ssh_user | SSH user to use with ansible | debian |

* **Controlplane variables**

| Variable name | Description | Default value |
| --- | --- | --- |
| name | Name of VM | master01-k3s |
| description | Description of VM | Controlplane 01 - Managed by Terraform |
| vmid | VM identifier | 150 |
| tags | Tags of VM | ["controlplane", "k3s", "debian", "terraform"] |
| cores | Allocated CPU to VM | 2 |
| memory | Allocated memory to VM | 2048 |
| disk_size | Allocated disk size to VM  | 15 |
| agent | Activate or not qemu-agent | true |
| ip | IP of VM | xxx.xxx.xxx.xxx/24 |
| gateway | Gateway to configure on VM | xxx.xxx.xxx.xxx |
| dns | DNS servers to configure | ["xxx.xxx.xxx.xxx"] |
| network_device | Network bridge to use from Proxmox node | vmbr0 |

* **Workernode variables**

| Variable name | Description | Default value |
| --- | --- | --- |
| name | Name of VM | worker01-k3s |
| description | Description of VM | Worker 01 - Managed by Terraform |
| vmid | VM identifier | 150 |
| tags | Tags of VM | ["worker", "k3s", "debian", "terraform"] |
| cores | Allocated CPU to VM | 4 |
| memory | Allocated memory to VM | 4096 |
| disk_size | Allocated disk size to VM  | 30 |
| agent | Activate or not qemu-agent | true |
| ip | IP of VM | xxx.xxx.xxx.xxx/24 |
| gateway | Gateway to configure on VM | xxx.xxx.xxx.xxx |
| dns | DNS servers to configure | ["xxx.xxx.xxx.xxx"] |
| network_device | Network bridge to use from Proxmox node | vmbr0 |

---

You will need to edit the file ansible/inventory/k3s-cluster/group_vars/all.yml.example :

* **Ansible inventory variables**

| Variable name | Description | Default value |
| --- | --- | --- |
| system_timezone | Set timezone on VM | Europe/Paris
| apiserver_endpoint | apiserver_endpoint is virtual ip-address which will be configured on each master (LAN NETWORK) | xxx.xxx.xxx.xxx |
| k3s_token | k3s_token is required  masters can talk together securely | mysecuredtoken |
| metal_lb_ip_range | metallb ip range for load balancer (LAN NETWORK) | xxx.xxx.xxx.x10-xxx.xxx.xxx.x20 |

## Start installation

```bash
bash launch.sh
```

## Destroy Cluster

```bash
bash reset.sh
```