#!/bin/bash
cd ansible
ansible-galaxy collection install -r ./collections/requirements.yml
cd ../terraform
terraform init --upgrade
terraform plan -var-file=variables.tfvars
terraform apply -var-file=variables.tfvars --auto-approve
cd ../ansible
ansible-playbook site.yml -i inventory/k3s-cluster/hosts