resource "tls_private_key" "ssh-key" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

resource "local_file" "ssh-key" {
  filename        = "ansible-key"
  content         = tls_private_key.ssh-key.private_key_pem
  file_permission = "0600"
}