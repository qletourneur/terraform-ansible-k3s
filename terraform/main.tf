provider "proxmox" {
  endpoint = var.proxmox_endpoint
  username = "${var.proxmox_user}@pam"
  password = var.proxmox_user_password

  # When using default self-signed certificate
  insecure = true

  ssh {
    agent = true
  }
}

terraform {
  required_providers {
    proxmox = {
      source  = "bpg/proxmox"
      version = "0.53.1"
    }
    random = {
      source  = "hashicorp/random"
      version = "3.6.0"
    }
    tls = {
      source  = "hashicorp/tls"
      version = "4.0.5"
    }
    local = {
      source  = "hashicorp/local"
      version = "2.5.1"
    }
  }
}

# Proxmox
variable "proxmox_endpoint" {
  type        = string
  description = "https + IP address or hostname of the Proxmox server + Port"
}

variable "proxmox_user" {
  type        = string
  description = "User to connect on Proxmox server"
}

variable "proxmox_user_password" {
  type        = string
  description = "User password to connect on Proxmox server"
  sensitive   = true
}

variable "proxmox_node" {
  type        = string
  description = "Name of node where you want things to be created"
}

variable "proxmox_datastore_images" {
  type        = string
  description = "Datastore to use on Proxmox for the images"
}

variable "proxmox_datastore_disks" {
  type        = string
  description = "Datastore to use on Proxmox for the VM disks"
}

variable "proxmox_datastore_files" {
  type        = string
  description = "Datastore to use on Proxmox for the files"
}

# K3s
variable "vm_ssh_public_key" {
  type        = string
  description = "SSH Public key to use in VM managed by Terraform"
}

variable "vm_ssh_user" {
  type        = string
  description = "SSH user to use with ansible"
}

variable "controlplane_config" {
  type = map(object({
    name           = string
    description    = string
    vmid           = number
    tags           = list(string)
    cores          = number
    memory         = number
    disk_size      = number
    agent          = bool
    ip             = string
    gateway        = string
    dns            = list(string)
    network_device = string
  }))
}

variable "workernode_config" {
  type = map(object({
    name           = string
    description    = string
    vmid           = number
    tags           = list(string)
    cores          = number
    memory         = number
    disk_size      = number
    agent          = bool
    ip             = string
    gateway        = string
    dns            = list(string)
    network_device = string
  }))
}