resource "proxmox_virtual_environment_file" "debian-cloudconfig" {
  content_type = "snippets"
  datastore_id = var.proxmox_datastore_files # This datastore must allow snippets file type Datacenter > Storage > `local` > Content = VZDump backup file, ISO image, Snippets, Container template
  node_name    = var.proxmox_node

  source_raw {
    data      = <<EOF
#cloud-config
apt_update: true
apt_upgrade: true
apt_reboot_if_required: true
packages:
  - qemu-guest-agent
  - python3

write_files:
 - path: /etc/ssh/sshd_config
   content: |
      PermitRootLogin no
      PubkeyAuthentication yes
      PasswordAuthentication no
      PermitEmptyPasswords no
      ChallengeResponseAuthentication no
      UsePAM yes
      X11Forwarding yes
      PrintMotd no
      AcceptEnv LANG LC_*
      Subsystem sftp    /usr/lib/openssh/sftp-server
users:
  - default
  - name: ${var.vm_ssh_user}
    groups: sudo
    shell: /bin/bash
    ssh-authorized-keys:
      - ${trimspace(var.vm_ssh_public_key)}
      - ${trimspace(tls_private_key.ssh-key.public_key_openssh)}
    sudo: ALL=(ALL) NOPASSWD:ALL
EOF
    file_name = "debian-cloudconfig.yaml"
  }
}

resource "proxmox_virtual_environment_vm" "controlplane_vm" {
  for_each        = var.controlplane_config
  name            = each.value.name
  description     = each.value.description
  tags            = each.value.tags
  template        = false
  node_name       = var.proxmox_node
  vm_id           = each.value.vmid
  keyboard_layout = "fr"

  agent {
    enabled = each.value.agent
  }

  startup {
    order      = "3"
    up_delay   = "60"
    down_delay = "60"
  }

  cpu {
    cores = each.value.cores
    #type  = "host"
  }

  memory {
    dedicated = each.value.memory
  }

  disk {
    datastore_id = var.proxmox_datastore_disks
    file_id      = proxmox_virtual_environment_download_file.debian_bookworm_20240211_cloudinit_qcow2_img.id
    interface    = "scsi0"
    size         = each.value.disk_size
  }

  initialization {
    ip_config {
      ipv4 {
        address = each.value.ip
        gateway = each.value.gateway
      }
    }
    dns {
      servers = each.value.dns
    }

    user_account {
      username = var.vm_ssh_user
      password = random_password.debian_vm_password.result
      keys     = [var.vm_ssh_public_key, tls_private_key.ssh-key.public_key_openssh]
    }

    user_data_file_id = proxmox_virtual_environment_file.debian-cloudconfig.id
  }

  network_device {
    bridge = each.value.network_device
  }

  operating_system {
    type = "l26"
  }

  depends_on = [
    random_password.debian_vm_password,
    proxmox_virtual_environment_file.debian-cloudconfig,
    proxmox_virtual_environment_download_file.debian_bookworm_20240211_cloudinit_qcow2_img
  ]
}

resource "proxmox_virtual_environment_vm" "worker_vm" {
  for_each        = var.workernode_config
  name            = each.value.name
  description     = each.value.description
  tags            = each.value.tags
  template        = false
  node_name       = var.proxmox_node
  vm_id           = each.value.vmid
  keyboard_layout = "fr"

  agent {
    enabled = each.value.agent
  }

  startup {
    order      = "3"
    up_delay   = "60"
    down_delay = "60"
  }

  cpu {
    cores = each.value.cores
    #type  = "host"
  }

  memory {
    dedicated = each.value.memory
  }

  disk {
    datastore_id = var.proxmox_datastore_disks
    file_id      = proxmox_virtual_environment_download_file.debian_bookworm_20240211_cloudinit_qcow2_img.id
    interface    = "scsi0"
    size         = each.value.disk_size
  }

  initialization {
    ip_config {
      ipv4 {
        address = each.value.ip
        gateway = each.value.gateway
      }
    }
    dns {
      servers = each.value.dns
    }

    user_account {
      username = var.vm_ssh_user
      password = random_password.debian_vm_password.result
      keys     = [var.vm_ssh_public_key, tls_private_key.ssh-key.public_key_openssh]
    }

    user_data_file_id = proxmox_virtual_environment_file.debian-cloudconfig.id
  }

  network_device {
    bridge = each.value.network_device
  }

  operating_system {
    type = "l26"
  }

  depends_on = [
    random_password.debian_vm_password,
    proxmox_virtual_environment_file.debian-cloudconfig,
    proxmox_virtual_environment_download_file.debian_bookworm_20240211_cloudinit_qcow2_img
  ]
}

resource "local_file" "tf_ansible_inventory_file" {
  content         = <<-EOF
[master]
${join("\n", [for name, config in var.controlplane_config : "${name} ansible_host=${split("/", config["ip"])[0]} ansible_user=${var.vm_ssh_user} ansible_ssh_private_key_file=../terraform/ansible-key ansible_ssh_common_args='-o StrictHostKeyChecking=no'"])}

[node]
${join("\n", [for name, config in var.workernode_config : "${name} ansible_host=${split("/", config["ip"])[0]} ansible_user=${var.vm_ssh_user} ansible_ssh_private_key_file=../terraform/ansible-key ansible_ssh_common_args='-o StrictHostKeyChecking=no'"])}

[k3s_cluster:children]
master
node
EOF
  filename        = "../ansible/inventory/k3s-cluster/hosts"
  file_permission = "0644"
}

resource "proxmox_virtual_environment_download_file" "debian_bookworm_20240211_cloudinit_qcow2_img" {
  content_type       = "iso"
  file_name          = "debian-12-generic-amd64-20240211-1654.img"
  datastore_id       = var.proxmox_datastore_images
  node_name          = var.proxmox_node
  url                = "http://cloud.debian.org/images/cloud/bookworm/20240211-1654/debian-12-generic-amd64-20240211-1654.qcow2"
  checksum           = "b679398972ba45a60574d9202c4f97ea647dd3577e857407138b73b71a3c3c039804e40aac2f877f3969676b6c8a1ebdb4f2d67a4efa6301c21e349e37d43ef5"
  checksum_algorithm = "sha512"
  upload_timeout     = "1200" # Timeout for download, 1200 is 20 minutes
}

resource "random_password" "debian_vm_password" {
  length           = 20
  override_special = "_%@"
  special          = true
}